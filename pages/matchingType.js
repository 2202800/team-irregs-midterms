//author: Juan Carlos Ugale
//array which holds the user's input
let correctMatchingArray = localStorage.getItem("correctAnsMatchingArray");
var userAnswer = [];
var quizJSON;


//author: Juan Carlos Ugale
//This function loads the question.json which then calls the
//respective function for the matching type section
async function loadMatchingType(file) {
    const request = new Request(file);
    const response = await fetch(request);
    quizJSON = await response.json();

    console.log(quizJSON);
    appendMatchingTypeQuestion(quizJSON);
}

function appendMatchingTypeQuestion(file) {
    var questions = file.questionsForMatching;

    for(let i = 0; i < 10; i++) {
        var labelMatch = document.createElement("label");
        var matchingQuestions = document.createElement("p");
        var matchingInput = document.createElement("div");
        var matchingBr = document.createElement("br");

        matchingInput.setAttribute("id", "match-input" + i);
        matchingInput.setAttribute("ondrop", "drop(event," + i + ")");
        matchingInput.setAttribute("ondragover", "allowDrop(event)");
        matchingInput.setAttribute("class", "droppable");
        matchingInput.textContent = "drop here!";


        matchingQuestions.setAttribute("class", "p-match-question");
        matchingQuestions.textContent = questions[i].matchingQuestion;

        document.getElementById("matchingTypeDiv").appendChild(labelMatch);
        document.getElementById("matchingTypeDiv").appendChild(matchingQuestions);
        document.getElementById("matchingTypeDiv").appendChild(matchingInput);
        document.getElementById("matchingTypeDiv").appendChild(matchingBr);
    }

    for(let i = 0; i < 10; i++) {
        var matchingSpan = document.createElement("span");

        matchingSpan.setAttribute("class", "draggable");
        matchingSpan.setAttribute("draggable", "true");
        matchingSpan.setAttribute("ondragstart", "drag(event)");
        matchingSpan.setAttribute("id", questions[i].matchingAnswer);
        matchingSpan.textContent = questions[i].matchingAnswer;

        document.getElementById("matchingTypeDiv").appendChild(matchingSpan);
    }
}

//author: Juan Carlos Ugale
//this function creates an event where it creates an area for
//the text or answer to be dropped in
function allowDrop(ev) {
        ev.preventDefault();
}
//author: Juan Carlos Ugale
//this function creates an event where the text is made draggable
function drag(ev) {
        ev.dataTransfer.setData("Text",ev.target.id);
}
//author: Juan Carlos Ugale
//this functions matches with the allowDrop event wherein it allows the
//draggable text to be dropped on the droppable area
function drop(ev, index) {
        ev.preventDefault();
        var data=ev.dataTransfer.getData("Text");
        ev.target.parentNode.replaceChild(document.getElementById(data), ev.target);
        document.getElementById(data).className = "";
        userAnswer[index] = data;
}
//author: Juan Carlos Ugale
//this function checks the input of the user by comparing the list
//of dropped text to the list of correct answers
function checkAnswer() {
    let correct = 0;
    const correctAnswer = ["Tim Cook","Satya Nadella","Mark Zuckerberg","Sundar Pichai","Andy Jassy","Ted Sarandos","Susan Wojcicki","Daniel Ek","Steve Huffman","Parag Agrawal"];

    for(var i = 0; i <= 9; i++) {
          if (userAnswer[i] == correctAnswer[i]) {
              console.log(userAnswer[i] + " TAMA " + correctAnswer[i])
              correct++;
          }
          else {
          console.log(userAnswer[i] + " MALI " + correctAnswer[i])
          }
    }
    correctMatchingArray = correctMatchingArray ? correctMatchingArray.split(',') : [];
    correctMatchingArray.push(correct);

    localStorage.setItem("correctAnsMatch", correct.toString());
    localStorage.setItem("correctAnsMatchingArray" , correctMatchingArray);
}