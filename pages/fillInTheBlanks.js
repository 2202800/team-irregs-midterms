let correctFillArray = localStorage.getItem("correctAnsFillArray");

// author: Julliard
// The function loads the question.json
// and calls another function inside to use the specific array from question.json
async function loadFillQuestions(jsonFile) {
    //const requestURL = 'http://localhost:63342/team-irregs-midterms/pages/fillInTheBlanks.json';
    const request = new Request(jsonFile);

    const response = await fetch(request);
    const questions = await response.json();

    console.log(questions);
    addFillInTheBlankQuestion(questions);
}

// author: Julliard
// The function creates element, set attributes, and set content.
// Then append the elements that has been created.
function addFillInTheBlankQuestion(file) {
    const questions = file.questionsForFill;

    for (let i = 1; i <= 10; i++) {
        const labelFill = document.createElement("label")
        const questionNumber = document.createElement("p");
        const fillQuestions = document.createElement("p");
        const inputElement = document.createElement("input");
        const spanElement = document.createElement("span");
        const brElement = document.createElement("br");

        labelFill.setAttribute("id", "id-fill"+i);
        inputElement.setAttribute("type", "text");
        inputElement.setAttribute("required", "required");
        inputElement.setAttribute("class", "input-box");
        inputElement.setAttribute("id", "idfq"+i);
        spanElement.setAttribute("class", "answer-span");
        spanElement.textContent = "Input Answer";

        questionNumber.setAttribute("class", "question-number");
        questionNumber.textContent = `${40 + i}.)`;

        fillQuestions.setAttribute("class", "p-fill-question")
        fillQuestions.textContent = questions[i - 1]['fillQuestion'];

        document.getElementById("id-form").appendChild(labelFill);
        document.getElementById("id-fill"+i).appendChild(questionNumber);
        document.getElementById("id-fill"+i).appendChild(inputElement);
        document.getElementById("id-fill"+i).appendChild(spanElement);
        document.getElementById("id-fill"+i).appendChild(fillQuestions);
        document.getElementById("id-fill"+i).appendChild(brElement);

        inputElement.value = localStorage.getItem(inputElement.getAttribute("id"));
        inputElement.addEventListener("keyup", event =>{
            localStorage.setItem(inputElement.getAttribute("id"), event.target.value);
        });
    }

}

function collectFillInTheBlankAnswer() {

}


// author: Julliard
// This function checks if the user's input is correct
async function checkFillAnswer() {
    const data = await response.json();
        let correctAns = 0;
        let userInput;
        let emptyInput = [];

        for (let i = 1; i <= 10; i++) {
            userInput = localStorage.getItem("idfq" + i);

            if (i==1) {
                if(userInput == null || userInput == "") {
            if (userInput == null || userInput == "") {
                emptyInput.push(40 + i)
            }
        }

                } else {
                    if (userInput.toUpperCase() == data['questionsForFill'][i - 1]['fillAnswer']) {
                        correctAns++;
        if (emptyInput === undefined || emptyInput.length == 0) {
            for (let i = 1; i <= 10; i++) {
                userInput = localStorage.getItem("idfq" + i);

                    }
                    localStorage.setItem("idfq" + i, "");
                }
            } else {
                if(userInput == null || userInput == "") {
                if (i==1) {
                    if(userInput == null || userInput == "") {

                    } else {
                        if (userInput.toUpperCase() == data['questionsForFill'][i - 1]['fillAnswer']) {
                            correctAns++;

                        }
                        localStorage.setItem("idfq" + i, "");
                    }
                } else {
                    userInput = userInput[0].toUpperCase() + userInput.substring(1);
                    if (userInput == data['questionsForFill'][i - 1]['fillAnswer']) {
                        correctAns++;
                    if(userInput == null || userInput == "") {

                    } else {
                        userInput = userInput[0].toUpperCase() + userInput.substring(1);
                        if (userInput == data['questionsForFill'][i - 1]['fillAnswer']) {
                            correctAns++;

                        }
                        localStorage.setItem("idfq" + i, "");
                    }
                    localStorage.setItem("idfq" + i, "");
                }
            }
        } else {
            checkEmptyInputs(emptyInput);

        }
        correctFillArray = correctFillArray ? correctFillArray.split(',') : [];
        correctFillArray.push(correctAns);

    localStorage.setItem("correctAnsFill", correctAns.toString());
    localStorage.setItem("correctAnsFillArray" , correctFillArray);
    //checkEmptyInputs(emptyInputs);
    //alert("You got " + correctAns);
}