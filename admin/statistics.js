




function login()
{
    var statisticsDiv = document.getElementById("statistics");
    var loginDiv = document.getElementById("login");
    var logoutDiv = document.getElementById("logout");

    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;

    // insert getting data from text file

    if (username == "admin" && password == "admin")
    {
     statisticsDiv.style.display = "block";
     loginDiv.style.display = "none";
     logoutDiv.style.display = "block";
    }

}

function logout()
{
     var statisticsDiv = document.getElementById("statistics");
     var loginDiv = document.getElementById("login");
     var logoutDiv = document.getElementById("logout");

     statisticsDiv.style.display = "none";
     loginDiv.style.display = "block";
     logoutDiv.style.display = "none";
}

// author: Julliard
// This function creates a graph based on the correct answers of the user
function drawBarGraph() {
    let totalScores= [];
    let getScoreMultiple = localStorage.getItem("correctAnsMultipleArray").split(',');
    let totalScoreMultiple = 0;
    let getScoreMatching = localStorage.getItem("correctAnsMatchingArray").split(',');
    let totalScoreMatching = 0;
    let getScoreFill = localStorage.getItem("correctAnsFillArray").split(',');
    let totalScoreFill = 0;

    for (var j = 0; j < getScoreMultiple.length; j++) {
        totalScoreMultiple = totalScoreMultiple + parseInt(getScoreMultiple[j]);
        totalScoreMatching = totalScoreMatching + parseInt(getScoreMatching[j]);
        totalScoreFill = totalScoreFill + parseInt(parseInt(getScoreFill[j]));
        
    }

    totalScores.push(totalScoreMultiple);
    totalScores.push(totalScoreMatching);
    totalScores.push(totalScoreFill);

    let graphValues = totalScores.splice(',');

    var canvas = document.getElementById("bar-graph");
    var ctx = canvas.getContext("2d");

    var x = 40;
    var width = 40;

    ctx.fillStyle = '#008080';

    for (var i = 0; i < graphValues.length; i++) {
        var h = graphValues[i];

        if (i==0) {
            ctx.fillRect(0, canvas.height - h, width, h);
            ctx.fillText(h, 0, canvas.height - 5);
        } else {
            ctx.fillRect(x, canvas.height - h, width, h);
            ctx.fillText(h, x, canvas.height - 5);
        }

        x += width + 60;
    }
}

// author: Julliard
// This function creates a graph using chartjs based on the correct answers of the user
function barGraphViaChart() {
    const questionLabels = ["Multiple Choice", "Matching Type", "Fill in the Blanks"];

    let totalScores= [];
    let getScoreMultiple = localStorage.getItem("correctAnsMultipleArray").split(',');
    let totalScoreMultiple = 0;
    let getScoreMatching = localStorage.getItem("correctAnsMatchingArray").split(',');
    let totalScoreMatching = 0;
    let getScoreFill = localStorage.getItem("correctAnsFillArray").split(',');
    let totalScoreFill = 0;

    for (var j = 0; j < getScoreMultiple.length; j++) {
        totalScoreMultiple = totalScoreMultiple + parseInt(getScoreMultiple[j]);
        totalScoreMatching = totalScoreMatching + parseInt(getScoreMatching[j]);
        totalScoreFill = totalScoreFill + parseInt(parseInt(getScoreFill[j]));

    }

    totalScores.push(totalScoreMultiple);
    totalScores.push(totalScoreMatching);
    totalScores.push(totalScoreFill);

    const ctx = document.getElementById("bar-graph-1").getContext("2d");
    const myChart = new Chart(ctx, {
        type: "bar",
        data: {
            labels: questionLabels,
            datasets: [{
                label: "Correct Answers Statistics",
                data: totalScores,
                backgroundColor: [
                    "rgba(255, 99, 132, 0.2)",
                    "rgba(54, 162, 235, 0.2)",
                    "rgba(255, 206, 86, 0.2)",
                ],
                borderColor: [
                    "rgba(255, 99, 132, 1)",
                    "rgba(54, 162, 235, 1)",
                    "rgba(255, 206, 86, 1)",
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
}

function clearData() {
    localStorage.clear();

}

